**[EN]** - [[IT]](./README.it.md)

This is bursa-pastoris' homepage.

## Contact

You are welcome to contact me about my software. However, if you wish to do so,
please _read this whole paragraph_ before you even open your email client:
there are some things you need to know in order to make sure your messages
reach me.

### Encryption

I may refuse to answer your messages if I think that this may compromise my
privacy. For example, I do not send unencrypted mail to Gmail or Outlook
addresses, as I do not trust Google and Microsoft not to use their users'
messages for nasty things like biometrical text analysis or AI training.

The universal solution for any such issue is encrypted email: encrypt your
message with my GPG key and attach yours, and we'll be fine. If you don't know
how to use email encryption, you will find the [Free Software Foundation guid
on the matter](https://emailselfdefense.fsf.org) very clear and useful.

Please note that I do _not_ distribute my key through keyservers. If you find
one there, do _not_ trust it, it's not mine!

### Message format

Please, make sure your messages are plain text, or at least include a plain
text version. HTML messages are are unnecessarily heavy, difficult to decode
and possibly dangerous to open. I will try my best to read HTML messages, but
I may not manage to do so, and even if I did I would be very upset.

If you format your message with stuff like italics text or in-body images, you
_do_ be using HTML, though probably your email client doesn't tell you. If you
do not format it, there is a good chance your email client is composing the
message in HTML, without any good reason for doing so.

For more information on the topic and some tips to configure your client
properly, see <https://plaintext.email>.

### Contact data

Since now you know the proper way to contact me, here's what you were actually
looking for:

- Email address: <bursapastoris@disroot.org>
- GPG key: [`2040 95ED C877 7412 EF7A  5A1F 07D3 4100 A2FA
1804`](https://git.disroot.org/bursa-pastoris/info/raw/branch/master/data/204095EDC8777412EF7A5A1F07D34100A2FA1804.asc)

## My "privacy policy"

When you contact me I will keep your message private and store it for as short
a time as possible. All the messages in my mailbox are removed from the server
the moment I download them.

You can read my email's provider privacy policy at
<https://disroot.org/en/privacy_policy>.

If you send me patches to any of my repositories and I choose to accept them, I
will merge your commits _as they are_, including committer and author name,
email address and date. Since you have full control over their content, I
assume you write them properly.
