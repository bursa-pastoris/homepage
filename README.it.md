[[EN]](./README.md) - **[IT]**

Questa è la pagina di bursa-pastoris.

## Contatti

Ricevo volentieri messaggi sul mio software. Tuttavia chiedo di leggere _tutto
questo paragrafo_ prima di aprire il client di posta elettronica: ci sono
alcune cose da sapere per essere sicuri che i messaggi mi raggiungano.

### Crittografia

Potrei rifiutare di rispondere se pensassi che questo possa compromettere la
mia riservatezza. Ad esempio, non invio email non criptate a indirizzo su Gmail
o Outlook perché non posso essere certo che Google e Microsoft non usino i
messaggi dei loro utenti per scopi che non condivido, come analisi biometrica
del testo o addestramento di intelligenze artificiali.

La soluzione universale per questi problemi sono le email criptate: basta
criptare i messaggi con la mia chiave GPG e allegare la propria. Chi non sa
usare le email criptate troverà utile la [guida della Free Software Foundation
sul tema](https://emailselfdefense.fsf.org), che è molto chiara.

È da tenere presente che _non_ distribuisco la mia chiave attraverso i
keyserver. Quelle eventualmente presenti non sono affidabili, _non_ sono mie.

### Formato dei messaggi

Assicurarsi che i messaggi siano in testo semplice, o almeno includano una
versione in testo semplice. I messaggi in HTML sono inutilmente pesanti,
difficili da decodificare e potenzialmente pericolosi da aprire. Faccio del mio
meglio per leggerli, ma potrei non riuscirci e anche se ce la facessi ne sarei
decisamente infastidito.

I messaggi formattati con cose come testo in corsivo o immagini nel corpo
_usano_ l'HTML, anche se probabilmente il client di posta elettronica non lo
dichiara. Ci sono buone probabilità che anche i messaggi non formattati vengano
composti in HTML, senza nessuna buona ragione.

Su <https://plaintext.email> sono disponibili più informazioni sul tema e
alcune indicazioni per impostare correttamente il proprio client.

### Informazioni di contatto

Ecco infine le informazioni che ci si aspettava di trovare:

- Indirizzo email: <bursapastoris@disroot.org>
- Chiave GPG: [`2040 95ED C877 7412 EF7A  5A1F 07D3 4100 A2FA
1804`](https://git.disroot.org/bursa-pastoris/info/raw/branch/master/data/204095EDC8777412EF7A5A1F07D34100A2FA1804.asc)

## La mia "informativa sulla privacy"

Mantengo riservati i messaggi e li conservo per il tempo più breve possibile.
Tutti i messaggi nella mia casella vengono cancellati dal server appena li
scarico.

L'informativa sulla privacy del mio fornitore di posta elettronica può essere
consultata su <https://disroot.org/en/privacy_policy>.

Quando decido di accettare una patch per un mio repository, faccio il merge
della patch _tal quale_, inclusi nome, indirizzo email e data di author e
committer. Dato che si ha pieno controllo sul contenuto, parto dal presupposto
che la patch venga creata correttamente.
